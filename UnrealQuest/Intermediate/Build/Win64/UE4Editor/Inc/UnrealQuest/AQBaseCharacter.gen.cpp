// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UnrealQuest/Character/AQBaseCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAQBaseCharacter() {}
// Cross Module References
	UNREALQUEST_API UClass* Z_Construct_UClass_AAQBaseCharacter_NoRegister();
	UNREALQUEST_API UClass* Z_Construct_UClass_AAQBaseCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_UnrealQuest();
	UNREALQUEST_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UAnimMontage_NoRegister();
	UNREALQUEST_API UClass* Z_Construct_UClass_UCharacterAttributeComponent_NoRegister();
// End Cross Module References
	void AAQBaseCharacter::StaticRegisterNativesAAQBaseCharacter()
	{
	}
	UClass* Z_Construct_UClass_AAQBaseCharacter_NoRegister()
	{
		return AAQBaseCharacter::StaticClass();
	}
	struct Z_Construct_UClass_AAQBaseCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LineOfSightObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_LineOfSightObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LineOfSightDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LineOfSightDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnDeathAnimMontage_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OnDeathAnimMontage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CharacterAttributeComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CharacterAttributeComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAQBaseCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_UnrealQuest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAQBaseCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Character/AQBaseCharacter.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Character/AQBaseCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_LineOfSightObject_MetaData[] = {
		{ "ModuleRelativePath", "Character/AQBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_LineOfSightObject = { "LineOfSightObject", nullptr, (EPropertyFlags)0x0024080000000000, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAQBaseCharacter, LineOfSightObject), Z_Construct_UClass_UInteractable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_LineOfSightObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_LineOfSightObject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_LineOfSightDistance_MetaData[] = {
		{ "Category", "Character | Trace" },
		{ "ModuleRelativePath", "Character/AQBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_LineOfSightDistance = { "LineOfSightDistance", nullptr, (EPropertyFlags)0x0020080000010015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAQBaseCharacter, LineOfSightDistance), METADATA_PARAMS(Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_LineOfSightDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_LineOfSightDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_OnDeathAnimMontage_MetaData[] = {
		{ "Category", "Character | Animations" },
		{ "ModuleRelativePath", "Character/AQBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_OnDeathAnimMontage = { "OnDeathAnimMontage", nullptr, (EPropertyFlags)0x0020080000010015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAQBaseCharacter, OnDeathAnimMontage), Z_Construct_UClass_UAnimMontage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_OnDeathAnimMontage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_OnDeathAnimMontage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_CharacterAttributeComponent_MetaData[] = {
		{ "Category", "Character | Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Character/AQBaseCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_CharacterAttributeComponent = { "CharacterAttributeComponent", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAQBaseCharacter, CharacterAttributeComponent), Z_Construct_UClass_UCharacterAttributeComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_CharacterAttributeComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_CharacterAttributeComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AAQBaseCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_LineOfSightObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_LineOfSightDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_OnDeathAnimMontage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAQBaseCharacter_Statics::NewProp_CharacterAttributeComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAQBaseCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAQBaseCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AAQBaseCharacter_Statics::ClassParams = {
		&AAQBaseCharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AAQBaseCharacter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AAQBaseCharacter_Statics::PropPointers),
		0,
		0x009000A5u,
		METADATA_PARAMS(Z_Construct_UClass_AAQBaseCharacter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AAQBaseCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAQBaseCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AAQBaseCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AAQBaseCharacter, 1783922533);
	template<> UNREALQUEST_API UClass* StaticClass<AAQBaseCharacter>()
	{
		return AAQBaseCharacter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AAQBaseCharacter(Z_Construct_UClass_AAQBaseCharacter, &AAQBaseCharacter::StaticClass, TEXT("/Script/UnrealQuest"), TEXT("AAQBaseCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAQBaseCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
