// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALQUEST_AQBaseCharacter_generated_h
#error "AQBaseCharacter.generated.h already included, missing '#pragma once' in AQBaseCharacter.h"
#endif
#define UNREALQUEST_AQBaseCharacter_generated_h

#define UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_SPARSE_DATA
#define UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_RPC_WRAPPERS
#define UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAQBaseCharacter(); \
	friend struct Z_Construct_UClass_AAQBaseCharacter_Statics; \
public: \
	DECLARE_CLASS(AAQBaseCharacter, ACharacter, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(AAQBaseCharacter)


#define UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAAQBaseCharacter(); \
	friend struct Z_Construct_UClass_AAQBaseCharacter_Statics; \
public: \
	DECLARE_CLASS(AAQBaseCharacter, ACharacter, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(AAQBaseCharacter)


#define UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAQBaseCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAQBaseCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAQBaseCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAQBaseCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAQBaseCharacter(AAQBaseCharacter&&); \
	NO_API AAQBaseCharacter(const AAQBaseCharacter&); \
public:


#define UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAQBaseCharacter(AAQBaseCharacter&&); \
	NO_API AAQBaseCharacter(const AAQBaseCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAQBaseCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAQBaseCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAQBaseCharacter)


#define UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CharacterAttributeComponent() { return STRUCT_OFFSET(AAQBaseCharacter, CharacterAttributeComponent); } \
	FORCEINLINE static uint32 __PPO__OnDeathAnimMontage() { return STRUCT_OFFSET(AAQBaseCharacter, OnDeathAnimMontage); } \
	FORCEINLINE static uint32 __PPO__LineOfSightDistance() { return STRUCT_OFFSET(AAQBaseCharacter, LineOfSightDistance); } \
	FORCEINLINE static uint32 __PPO__LineOfSightObject() { return STRUCT_OFFSET(AAQBaseCharacter, LineOfSightObject); }


#define UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_13_PROLOG
#define UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_RPC_WRAPPERS \
	UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_INCLASS \
	UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_INCLASS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALQUEST_API UClass* StaticClass<class AAQBaseCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealQuest_Source_UnrealQuest_Character_AQBaseCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
