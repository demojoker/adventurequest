// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALQUEST_AQBaseCharacterAnimInstance_generated_h
#error "AQBaseCharacterAnimInstance.generated.h already included, missing '#pragma once' in AQBaseCharacterAnimInstance.h"
#endif
#define UNREALQUEST_AQBaseCharacterAnimInstance_generated_h

#define UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_SPARSE_DATA
#define UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_RPC_WRAPPERS
#define UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAQBaseCharacterAnimInstance(); \
	friend struct Z_Construct_UClass_UAQBaseCharacterAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UAQBaseCharacterAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(UAQBaseCharacterAnimInstance)


#define UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUAQBaseCharacterAnimInstance(); \
	friend struct Z_Construct_UClass_UAQBaseCharacterAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UAQBaseCharacterAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(UAQBaseCharacterAnimInstance)


#define UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAQBaseCharacterAnimInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAQBaseCharacterAnimInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAQBaseCharacterAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAQBaseCharacterAnimInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAQBaseCharacterAnimInstance(UAQBaseCharacterAnimInstance&&); \
	NO_API UAQBaseCharacterAnimInstance(const UAQBaseCharacterAnimInstance&); \
public:


#define UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAQBaseCharacterAnimInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAQBaseCharacterAnimInstance(UAQBaseCharacterAnimInstance&&); \
	NO_API UAQBaseCharacterAnimInstance(const UAQBaseCharacterAnimInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAQBaseCharacterAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAQBaseCharacterAnimInstance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAQBaseCharacterAnimInstance)


#define UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Speed() { return STRUCT_OFFSET(UAQBaseCharacterAnimInstance, Speed); } \
	FORCEINLINE static uint32 __PPO__bIsFalling() { return STRUCT_OFFSET(UAQBaseCharacterAnimInstance, bIsFalling); }


#define UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_12_PROLOG
#define UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_RPC_WRAPPERS \
	UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_INCLASS \
	UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_INCLASS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALQUEST_API UClass* StaticClass<class UAQBaseCharacterAnimInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealQuest_Source_UnrealQuest_Character_Animations_AQBaseCharacterAnimInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
