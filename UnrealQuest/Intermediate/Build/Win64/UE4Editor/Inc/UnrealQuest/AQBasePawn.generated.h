// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALQUEST_AQBasePawn_generated_h
#error "AQBasePawn.generated.h already included, missing '#pragma once' in AQBasePawn.h"
#endif
#define UNREALQUEST_AQBasePawn_generated_h

#define UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_SPARSE_DATA
#define UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_RPC_WRAPPERS
#define UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAQBasePawn(); \
	friend struct Z_Construct_UClass_AAQBasePawn_Statics; \
public: \
	DECLARE_CLASS(AAQBasePawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(AAQBasePawn)


#define UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAAQBasePawn(); \
	friend struct Z_Construct_UClass_AAQBasePawn_Statics; \
public: \
	DECLARE_CLASS(AAQBasePawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(AAQBasePawn)


#define UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAQBasePawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAQBasePawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAQBasePawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAQBasePawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAQBasePawn(AAQBasePawn&&); \
	NO_API AAQBasePawn(const AAQBasePawn&); \
public:


#define UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAQBasePawn(AAQBasePawn&&); \
	NO_API AAQBasePawn(const AAQBasePawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAQBasePawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAQBasePawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAQBasePawn)


#define UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_PRIVATE_PROPERTY_OFFSET
#define UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_9_PROLOG
#define UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_RPC_WRAPPERS \
	UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_INCLASS \
	UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_INCLASS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALQUEST_API UClass* StaticClass<class AAQBasePawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealQuest_Source_UnrealQuest_Character_AQBasePawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
