// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UnrealQuest/Components/MovementComponents/AQBasePawnMovementComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAQBasePawnMovementComponent() {}
// Cross Module References
	UNREALQUEST_API UClass* Z_Construct_UClass_UAQBasePawnMovementComponent_NoRegister();
	UNREALQUEST_API UClass* Z_Construct_UClass_UAQBasePawnMovementComponent();
	ENGINE_API UClass* Z_Construct_UClass_UPawnMovementComponent();
	UPackage* Z_Construct_UPackage__Script_UnrealQuest();
// End Cross Module References
	void UAQBasePawnMovementComponent::StaticRegisterNativesUAQBasePawnMovementComponent()
	{
	}
	UClass* Z_Construct_UClass_UAQBasePawnMovementComponent_NoRegister()
	{
		return UAQBasePawnMovementComponent::StaticClass();
	}
	struct Z_Construct_UClass_UAQBasePawnMovementComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableGravity_MetaData[];
#endif
		static void NewProp_bEnableGravity_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableGravity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxSpeed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPawnMovementComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_UnrealQuest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Components/MovementComponents/AQBasePawnMovementComponent.h" },
		{ "ModuleRelativePath", "Components/MovementComponents/AQBasePawnMovementComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::NewProp_bEnableGravity_MetaData[] = {
		{ "Category", "AQBasePawnMovementComponent" },
		{ "ModuleRelativePath", "Components/MovementComponents/AQBasePawnMovementComponent.h" },
	};
#endif
	void Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::NewProp_bEnableGravity_SetBit(void* Obj)
	{
		((UAQBasePawnMovementComponent*)Obj)->bEnableGravity = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::NewProp_bEnableGravity = { "bEnableGravity", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAQBasePawnMovementComponent), &Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::NewProp_bEnableGravity_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::NewProp_bEnableGravity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::NewProp_bEnableGravity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::NewProp_MaxSpeed_MetaData[] = {
		{ "Category", "AQBasePawnMovementComponent" },
		{ "ModuleRelativePath", "Components/MovementComponents/AQBasePawnMovementComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::NewProp_MaxSpeed = { "MaxSpeed", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAQBasePawnMovementComponent, MaxSpeed), METADATA_PARAMS(Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::NewProp_MaxSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::NewProp_MaxSpeed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::NewProp_bEnableGravity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::NewProp_MaxSpeed,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAQBasePawnMovementComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::ClassParams = {
		&UAQBasePawnMovementComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAQBasePawnMovementComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAQBasePawnMovementComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAQBasePawnMovementComponent, 2529488349);
	template<> UNREALQUEST_API UClass* StaticClass<UAQBasePawnMovementComponent>()
	{
		return UAQBasePawnMovementComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAQBasePawnMovementComponent(Z_Construct_UClass_UAQBasePawnMovementComponent, &UAQBasePawnMovementComponent::StaticClass, TEXT("/Script/UnrealQuest"), TEXT("UAQBasePawnMovementComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAQBasePawnMovementComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
