// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALQUEST_AQBasePawnMovementComponent_generated_h
#error "AQBasePawnMovementComponent.generated.h already included, missing '#pragma once' in AQBasePawnMovementComponent.h"
#endif
#define UNREALQUEST_AQBasePawnMovementComponent_generated_h

#define UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_SPARSE_DATA
#define UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_RPC_WRAPPERS
#define UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAQBasePawnMovementComponent(); \
	friend struct Z_Construct_UClass_UAQBasePawnMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UAQBasePawnMovementComponent, UPawnMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(UAQBasePawnMovementComponent)


#define UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUAQBasePawnMovementComponent(); \
	friend struct Z_Construct_UClass_UAQBasePawnMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UAQBasePawnMovementComponent, UPawnMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(UAQBasePawnMovementComponent)


#define UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAQBasePawnMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAQBasePawnMovementComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAQBasePawnMovementComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAQBasePawnMovementComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAQBasePawnMovementComponent(UAQBasePawnMovementComponent&&); \
	NO_API UAQBasePawnMovementComponent(const UAQBasePawnMovementComponent&); \
public:


#define UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAQBasePawnMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAQBasePawnMovementComponent(UAQBasePawnMovementComponent&&); \
	NO_API UAQBasePawnMovementComponent(const UAQBasePawnMovementComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAQBasePawnMovementComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAQBasePawnMovementComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAQBasePawnMovementComponent)


#define UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MaxSpeed() { return STRUCT_OFFSET(UAQBasePawnMovementComponent, MaxSpeed); } \
	FORCEINLINE static uint32 __PPO__bEnableGravity() { return STRUCT_OFFSET(UAQBasePawnMovementComponent, bEnableGravity); }


#define UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_12_PROLOG
#define UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_RPC_WRAPPERS \
	UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_INCLASS \
	UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_INCLASS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALQUEST_API UClass* StaticClass<class UAQBasePawnMovementComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealQuest_Source_UnrealQuest_Components_MovementComponents_AQBasePawnMovementComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
