// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALQUEST_AQPlayerController_generated_h
#error "AQPlayerController.generated.h already included, missing '#pragma once' in AQPlayerController.h"
#endif
#define UNREALQUEST_AQPlayerController_generated_h

#define UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_SPARSE_DATA
#define UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_RPC_WRAPPERS
#define UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAQPlayerController(); \
	friend struct Z_Construct_UClass_AAQPlayerController_Statics; \
public: \
	DECLARE_CLASS(AAQPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(AAQPlayerController)


#define UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAAQPlayerController(); \
	friend struct Z_Construct_UClass_AAQPlayerController_Statics; \
public: \
	DECLARE_CLASS(AAQPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(AAQPlayerController)


#define UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAQPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAQPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAQPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAQPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAQPlayerController(AAQPlayerController&&); \
	NO_API AAQPlayerController(const AAQPlayerController&); \
public:


#define UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAQPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAQPlayerController(AAQPlayerController&&); \
	NO_API AAQPlayerController(const AAQPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAQPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAQPlayerController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAQPlayerController)


#define UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_PRIVATE_PROPERTY_OFFSET
#define UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_12_PROLOG
#define UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_RPC_WRAPPERS \
	UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_INCLASS \
	UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_INCLASS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALQUEST_API UClass* StaticClass<class AAQPlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealQuest_Source_UnrealQuest_Character_Controllers_AQPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
