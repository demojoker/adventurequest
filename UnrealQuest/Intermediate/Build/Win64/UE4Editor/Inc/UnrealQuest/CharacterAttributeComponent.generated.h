// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UDamageType;
class AController;
#ifdef UNREALQUEST_CharacterAttributeComponent_generated_h
#error "CharacterAttributeComponent.generated.h already included, missing '#pragma once' in CharacterAttributeComponent.h"
#endif
#define UNREALQUEST_CharacterAttributeComponent_generated_h

#define UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_SPARSE_DATA
#define UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnTakeAnyDamage);


#define UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnTakeAnyDamage);


#define UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCharacterAttributeComponent(); \
	friend struct Z_Construct_UClass_UCharacterAttributeComponent_Statics; \
public: \
	DECLARE_CLASS(UCharacterAttributeComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(UCharacterAttributeComponent)


#define UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUCharacterAttributeComponent(); \
	friend struct Z_Construct_UClass_UCharacterAttributeComponent_Statics; \
public: \
	DECLARE_CLASS(UCharacterAttributeComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(UCharacterAttributeComponent)


#define UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCharacterAttributeComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCharacterAttributeComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCharacterAttributeComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCharacterAttributeComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCharacterAttributeComponent(UCharacterAttributeComponent&&); \
	NO_API UCharacterAttributeComponent(const UCharacterAttributeComponent&); \
public:


#define UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCharacterAttributeComponent(UCharacterAttributeComponent&&); \
	NO_API UCharacterAttributeComponent(const UCharacterAttributeComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCharacterAttributeComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCharacterAttributeComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCharacterAttributeComponent)


#define UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MaxHealth() { return STRUCT_OFFSET(UCharacterAttributeComponent, MaxHealth); }


#define UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_11_PROLOG
#define UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_RPC_WRAPPERS \
	UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_INCLASS \
	UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_INCLASS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALQUEST_API UClass* StaticClass<class UCharacterAttributeComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealQuest_Source_UnrealQuest_Components_CharacterComponents_CharacterAttributeComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
