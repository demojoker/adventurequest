// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALQUEST_Chest_generated_h
#error "Chest.generated.h already included, missing '#pragma once' in Chest.h"
#endif
#define UNREALQUEST_Chest_generated_h

#define UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_SPARSE_DATA
#define UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_RPC_WRAPPERS
#define UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAChest(); \
	friend struct Z_Construct_UClass_AChest_Statics; \
public: \
	DECLARE_CLASS(AChest, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(AChest)


#define UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAChest(); \
	friend struct Z_Construct_UClass_AChest_Statics; \
public: \
	DECLARE_CLASS(AChest, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(AChest)


#define UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AChest(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AChest) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AChest); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AChest); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AChest(AChest&&); \
	NO_API AChest(const AChest&); \
public:


#define UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AChest(AChest&&); \
	NO_API AChest(const AChest&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AChest); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AChest); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AChest)


#define UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_PRIVATE_PROPERTY_OFFSET
#define UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_9_PROLOG
#define UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_RPC_WRAPPERS \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_INCLASS \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_INCLASS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALQUEST_API UClass* StaticClass<class AChest>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealQuest_Source_UnrealQuest_Actor_Environment_Chest_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
