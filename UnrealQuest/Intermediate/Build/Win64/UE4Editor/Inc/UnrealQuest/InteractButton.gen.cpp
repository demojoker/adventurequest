// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UnrealQuest/Actor/Environment/InteractButton.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeInteractButton() {}
// Cross Module References
	UNREALQUEST_API UClass* Z_Construct_UClass_AInteractButton_NoRegister();
	UNREALQUEST_API UClass* Z_Construct_UClass_AInteractButton();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_UnrealQuest();
	ENGINE_API UClass* Z_Construct_UClass_UCurveFloat_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	UNREALQUEST_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AInteractButton::execOnButtonAnimationFinished)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnButtonAnimationFinished();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AInteractButton::execUpdateButtonAnimation)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_Alpha);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->UpdateButtonAnimation(Z_Param_Alpha);
		P_NATIVE_END;
	}
	void AInteractButton::StaticRegisterNativesAInteractButton()
	{
		UClass* Class = AInteractButton::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnButtonAnimationFinished", &AInteractButton::execOnButtonAnimationFinished },
			{ "UpdateButtonAnimation", &AInteractButton::execUpdateButtonAnimation },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AInteractButton_OnButtonAnimationFinished_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AInteractButton_OnButtonAnimationFinished_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Actor/Environment/InteractButton.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AInteractButton_OnButtonAnimationFinished_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AInteractButton, nullptr, "OnButtonAnimationFinished", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AInteractButton_OnButtonAnimationFinished_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AInteractButton_OnButtonAnimationFinished_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AInteractButton_OnButtonAnimationFinished()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AInteractButton_OnButtonAnimationFinished_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AInteractButton_UpdateButtonAnimation_Statics
	{
		struct InteractButton_eventUpdateButtonAnimation_Parms
		{
			float Alpha;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Alpha;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AInteractButton_UpdateButtonAnimation_Statics::NewProp_Alpha = { "Alpha", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(InteractButton_eventUpdateButtonAnimation_Parms, Alpha), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AInteractButton_UpdateButtonAnimation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AInteractButton_UpdateButtonAnimation_Statics::NewProp_Alpha,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AInteractButton_UpdateButtonAnimation_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Actor/Environment/InteractButton.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AInteractButton_UpdateButtonAnimation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AInteractButton, nullptr, "UpdateButtonAnimation", nullptr, nullptr, sizeof(InteractButton_eventUpdateButtonAnimation_Parms), Z_Construct_UFunction_AInteractButton_UpdateButtonAnimation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AInteractButton_UpdateButtonAnimation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AInteractButton_UpdateButtonAnimation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AInteractButton_UpdateButtonAnimation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AInteractButton_UpdateButtonAnimation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AInteractButton_UpdateButtonAnimation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AInteractButton_NoRegister()
	{
		return AInteractButton::StaticClass();
	}
	struct Z_Construct_UClass_AInteractButton_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ButtonAnimationCurve_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ButtonAnimationCurve;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PressedBtn_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PressedBtn;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NonPressedBtn_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NonPressedBtn;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ButtonPressing_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ButtonPressing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ButtonMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ButtonMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AInteractButton_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_UnrealQuest,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AInteractButton_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AInteractButton_OnButtonAnimationFinished, "OnButtonAnimationFinished" }, // 1672372524
		{ &Z_Construct_UFunction_AInteractButton_UpdateButtonAnimation, "UpdateButtonAnimation" }, // 3195406964
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AInteractButton_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Actor/Environment/InteractButton.h" },
		{ "ModuleRelativePath", "Actor/Environment/InteractButton.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AInteractButton_Statics::NewProp_ButtonAnimationCurve_MetaData[] = {
		{ "Category", "Interactive | Button" },
		{ "ModuleRelativePath", "Actor/Environment/InteractButton.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AInteractButton_Statics::NewProp_ButtonAnimationCurve = { "ButtonAnimationCurve", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AInteractButton, ButtonAnimationCurve), Z_Construct_UClass_UCurveFloat_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AInteractButton_Statics::NewProp_ButtonAnimationCurve_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AInteractButton_Statics::NewProp_ButtonAnimationCurve_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AInteractButton_Statics::NewProp_PressedBtn_MetaData[] = {
		{ "Category", "Interactive | Button" },
		{ "ModuleRelativePath", "Actor/Environment/InteractButton.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AInteractButton_Statics::NewProp_PressedBtn = { "PressedBtn", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AInteractButton, PressedBtn), METADATA_PARAMS(Z_Construct_UClass_AInteractButton_Statics::NewProp_PressedBtn_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AInteractButton_Statics::NewProp_PressedBtn_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AInteractButton_Statics::NewProp_NonPressedBtn_MetaData[] = {
		{ "Category", "Interactive | Button" },
		{ "ModuleRelativePath", "Actor/Environment/InteractButton.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AInteractButton_Statics::NewProp_NonPressedBtn = { "NonPressedBtn", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AInteractButton, NonPressedBtn), METADATA_PARAMS(Z_Construct_UClass_AInteractButton_Statics::NewProp_NonPressedBtn_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AInteractButton_Statics::NewProp_NonPressedBtn_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AInteractButton_Statics::NewProp_ButtonPressing_MetaData[] = {
		{ "Category", "Interactive | Button" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Actor/Environment/InteractButton.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AInteractButton_Statics::NewProp_ButtonPressing = { "ButtonPressing", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AInteractButton, ButtonPressing), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AInteractButton_Statics::NewProp_ButtonPressing_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AInteractButton_Statics::NewProp_ButtonPressing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AInteractButton_Statics::NewProp_ButtonMesh_MetaData[] = {
		{ "Category", "Interactive | Button" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Actor/Environment/InteractButton.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AInteractButton_Statics::NewProp_ButtonMesh = { "ButtonMesh", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AInteractButton, ButtonMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AInteractButton_Statics::NewProp_ButtonMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AInteractButton_Statics::NewProp_ButtonMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AInteractButton_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AInteractButton_Statics::NewProp_ButtonAnimationCurve,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AInteractButton_Statics::NewProp_PressedBtn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AInteractButton_Statics::NewProp_NonPressedBtn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AInteractButton_Statics::NewProp_ButtonPressing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AInteractButton_Statics::NewProp_ButtonMesh,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AInteractButton_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(AInteractButton, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AInteractButton_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AInteractButton>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AInteractButton_Statics::ClassParams = {
		&AInteractButton::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AInteractButton_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AInteractButton_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AInteractButton_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AInteractButton_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AInteractButton()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AInteractButton_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AInteractButton, 896070585);
	template<> UNREALQUEST_API UClass* StaticClass<AInteractButton>()
	{
		return AInteractButton::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AInteractButton(Z_Construct_UClass_AInteractButton, &AInteractButton::StaticClass, TEXT("/Script/UnrealQuest"), TEXT("AInteractButton"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AInteractButton);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
