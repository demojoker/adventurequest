// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALQUEST_InteractButton_generated_h
#error "InteractButton.generated.h already included, missing '#pragma once' in InteractButton.h"
#endif
#define UNREALQUEST_InteractButton_generated_h

#define UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_SPARSE_DATA
#define UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnButtonAnimationFinished); \
	DECLARE_FUNCTION(execUpdateButtonAnimation);


#define UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnButtonAnimationFinished); \
	DECLARE_FUNCTION(execUpdateButtonAnimation);


#define UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAInteractButton(); \
	friend struct Z_Construct_UClass_AInteractButton_Statics; \
public: \
	DECLARE_CLASS(AInteractButton, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(AInteractButton) \
	virtual UObject* _getUObject() const override { return const_cast<AInteractButton*>(this); }


#define UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAInteractButton(); \
	friend struct Z_Construct_UClass_AInteractButton_Statics; \
public: \
	DECLARE_CLASS(AInteractButton, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(AInteractButton) \
	virtual UObject* _getUObject() const override { return const_cast<AInteractButton*>(this); }


#define UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AInteractButton(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AInteractButton) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AInteractButton); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AInteractButton); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AInteractButton(AInteractButton&&); \
	NO_API AInteractButton(const AInteractButton&); \
public:


#define UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AInteractButton(AInteractButton&&); \
	NO_API AInteractButton(const AInteractButton&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AInteractButton); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AInteractButton); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AInteractButton)


#define UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ButtonMesh() { return STRUCT_OFFSET(AInteractButton, ButtonMesh); } \
	FORCEINLINE static uint32 __PPO__ButtonPressing() { return STRUCT_OFFSET(AInteractButton, ButtonPressing); } \
	FORCEINLINE static uint32 __PPO__NonPressedBtn() { return STRUCT_OFFSET(AInteractButton, NonPressedBtn); } \
	FORCEINLINE static uint32 __PPO__PressedBtn() { return STRUCT_OFFSET(AInteractButton, PressedBtn); } \
	FORCEINLINE static uint32 __PPO__ButtonAnimationCurve() { return STRUCT_OFFSET(AInteractButton, ButtonAnimationCurve); }


#define UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_12_PROLOG
#define UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_RPC_WRAPPERS \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_INCLASS \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_INCLASS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALQUEST_API UClass* StaticClass<class AInteractButton>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealQuest_Source_UnrealQuest_Actor_Environment_InteractButton_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
