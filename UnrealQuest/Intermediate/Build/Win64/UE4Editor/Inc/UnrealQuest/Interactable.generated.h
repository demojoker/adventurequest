// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALQUEST_Interactable_generated_h
#error "Interactable.generated.h already included, missing '#pragma once' in Interactable.h"
#endif
#define UNREALQUEST_Interactable_generated_h

#define UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_SPARSE_DATA
#define UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_RPC_WRAPPERS
#define UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	UNREALQUEST_API UInteractable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(UNREALQUEST_API, UInteractable); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	UNREALQUEST_API UInteractable(UInteractable&&); \
	UNREALQUEST_API UInteractable(const UInteractable&); \
public:


#define UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	UNREALQUEST_API UInteractable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	UNREALQUEST_API UInteractable(UInteractable&&); \
	UNREALQUEST_API UInteractable(const UInteractable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(UNREALQUEST_API, UInteractable); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractable); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractable)


#define UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUInteractable(); \
	friend struct Z_Construct_UClass_UInteractable_Statics; \
public: \
	DECLARE_CLASS(UInteractable, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/UnrealQuest"), UNREALQUEST_API) \
	DECLARE_SERIALIZER(UInteractable)


#define UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_GENERATED_UINTERFACE_BODY() \
	UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_GENERATED_UINTERFACE_BODY() \
	UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IInteractable() {} \
public: \
	typedef UInteractable UClassType; \
	typedef IInteractable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_INCLASS_IINTERFACE \
protected: \
	virtual ~IInteractable() {} \
public: \
	typedef UInteractable UClassType; \
	typedef IInteractable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_11_PROLOG
#define UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_RPC_WRAPPERS \
	UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h_14_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALQUEST_API UClass* StaticClass<class UInteractable>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealQuest_Source_UnrealQuest_Actor_Interactive_Interface_Interactable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
