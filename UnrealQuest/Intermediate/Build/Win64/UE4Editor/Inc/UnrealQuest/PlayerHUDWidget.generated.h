// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALQUEST_PlayerHUDWidget_generated_h
#error "PlayerHUDWidget.generated.h already included, missing '#pragma once' in PlayerHUDWidget.h"
#endif
#define UNREALQUEST_PlayerHUDWidget_generated_h

#define UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_SPARSE_DATA
#define UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetHealthPercent);


#define UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetHealthPercent);


#define UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlayerHUDWidget(); \
	friend struct Z_Construct_UClass_UPlayerHUDWidget_Statics; \
public: \
	DECLARE_CLASS(UPlayerHUDWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(UPlayerHUDWidget)


#define UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUPlayerHUDWidget(); \
	friend struct Z_Construct_UClass_UPlayerHUDWidget_Statics; \
public: \
	DECLARE_CLASS(UPlayerHUDWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(UPlayerHUDWidget)


#define UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlayerHUDWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlayerHUDWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerHUDWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerHUDWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerHUDWidget(UPlayerHUDWidget&&); \
	NO_API UPlayerHUDWidget(const UPlayerHUDWidget&); \
public:


#define UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlayerHUDWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerHUDWidget(UPlayerHUDWidget&&); \
	NO_API UPlayerHUDWidget(const UPlayerHUDWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerHUDWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerHUDWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlayerHUDWidget)


#define UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_PRIVATE_PROPERTY_OFFSET
#define UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_12_PROLOG
#define UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_RPC_WRAPPERS \
	UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_INCLASS \
	UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_INCLASS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALQUEST_API UClass* StaticClass<class UPlayerHUDWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealQuest_Source_UnrealQuest_UI_PlayerHUDWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
