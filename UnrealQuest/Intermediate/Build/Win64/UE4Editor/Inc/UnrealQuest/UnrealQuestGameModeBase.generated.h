// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALQUEST_UnrealQuestGameModeBase_generated_h
#error "UnrealQuestGameModeBase.generated.h already included, missing '#pragma once' in UnrealQuestGameModeBase.h"
#endif
#define UNREALQUEST_UnrealQuestGameModeBase_generated_h

#define UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_SPARSE_DATA
#define UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_RPC_WRAPPERS
#define UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAUnrealQuestGameModeBase(); \
	friend struct Z_Construct_UClass_AUnrealQuestGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AUnrealQuestGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(AUnrealQuestGameModeBase)


#define UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAUnrealQuestGameModeBase(); \
	friend struct Z_Construct_UClass_AUnrealQuestGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AUnrealQuestGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealQuest"), NO_API) \
	DECLARE_SERIALIZER(AUnrealQuestGameModeBase)


#define UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUnrealQuestGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUnrealQuestGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnrealQuestGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnrealQuestGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnrealQuestGameModeBase(AUnrealQuestGameModeBase&&); \
	NO_API AUnrealQuestGameModeBase(const AUnrealQuestGameModeBase&); \
public:


#define UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUnrealQuestGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnrealQuestGameModeBase(AUnrealQuestGameModeBase&&); \
	NO_API AUnrealQuestGameModeBase(const AUnrealQuestGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnrealQuestGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnrealQuestGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUnrealQuestGameModeBase)


#define UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_12_PROLOG
#define UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_RPC_WRAPPERS \
	UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_INCLASS \
	UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_SPARSE_DATA \
	UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALQUEST_API UClass* StaticClass<class AUnrealQuestGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealQuest_Source_UnrealQuest_UnrealQuestGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
