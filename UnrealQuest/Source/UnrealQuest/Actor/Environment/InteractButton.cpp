// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractButton.h"
#include "Components/TimelineComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AInteractButton::AInteractButton()
{
	USceneComponent* DefaultSceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("ButtonRoot"));
	SetRootComponent(DefaultSceneRoot);

	ButtonPressing = CreateDefaultSubobject<USceneComponent>(TEXT("ButtonPressing"));
	ButtonPressing->SetupAttachment(GetRootComponent());

	ButtonMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ButtonMesh"));
	ButtonMesh->SetupAttachment(ButtonPressing);

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
}

void AInteractButton::Interact(AAQBaseCharacter* Character)
{
	ensureMsgf(IsValid(ButtonAnimationCurve), TEXT("Button animation curve is not set"));
	InteractWithButton();
}

// Called when the game starts or when spawned
void AInteractButton::BeginPlay()
{
	Super::BeginPlay();
	if (IsValid(ButtonAnimationCurve))
	{
		FOnTimelineFloatStatic ButtonAnimationDelegate;
		ButtonAnimationDelegate.BindUObject(this, &AInteractButton::UpdateButtonAnimation);
		ButtonPressingAnimTimeline.AddInterpFloat(ButtonAnimationCurve, ButtonAnimationDelegate);

		FOnTimelineEventStatic ButtonPressedDelegate;
		ButtonPressedDelegate.BindUObject(this, &AInteractButton::OnButtonAnimationFinished);
		ButtonPressingAnimTimeline.SetTimelineFinishedFunc(ButtonPressedDelegate);
	}
	
}

void AInteractButton::InteractWithButton()
{
	SetActorTickEnabled(true);
	if (bIsPressed)
	{
		ButtonPressingAnimTimeline.Reverse();
	}
	else
	{
		ButtonPressingAnimTimeline.Play();
	}
	bIsPressed = !bIsPressed;
}

void AInteractButton::UpdateButtonAnimation(float Alpha)
{
	float PressingTheButton = FMath::Lerp(NonPressedBtn, PressedBtn, FMath::Clamp(Alpha, 0.0f, 1.0f));
	ButtonPressing->SetRelativeRotation(FRotator(PressingTheButton, 0.0f, 0.0f));
}

void AInteractButton::OnButtonAnimationFinished()
{
	SetActorTickEnabled(false);
}

FName AInteractButton::GetActionEventName() const
{
	return FName("Interact");
}

// Called every frame
void AInteractButton::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	ButtonPressingAnimTimeline.TickTimeline(DeltaTime);
}

