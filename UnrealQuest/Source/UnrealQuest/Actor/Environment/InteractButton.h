// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Interactive/Interface/Interactable.h"
#include "Components/TimelineComponent.h"
#include "InteractButton.generated.h"


UCLASS()
class UNREALQUEST_API AInteractButton : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	AInteractButton();

	virtual void Interact(AAQBaseCharacter* Character) override;

	virtual FName GetActionEventName() const override;

	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Interactive | Button")
	UStaticMeshComponent* ButtonMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Interactive | Button")
	USceneComponent* ButtonPressing;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Interactive | Button")
	float NonPressedBtn = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Interactive | Button")
	float PressedBtn = -100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Interactive | Button")
	UCurveFloat* ButtonAnimationCurve;


private:
	void InteractWithButton();

	UFUNCTION()
	void UpdateButtonAnimation(float Alpha);

	UFUNCTION()
	void OnButtonAnimationFinished();

	FTimeline ButtonPressingAnimTimeline;

	bool bIsPressed = false;

};
