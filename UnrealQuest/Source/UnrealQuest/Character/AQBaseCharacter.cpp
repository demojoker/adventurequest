// Fill out your copyright notice in the Description page of Project Settings.


#include "AQBaseCharacter.h"
#include "../Components/CharacterComponents/CharacterAttributeComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "../Actor/Interactive/Interface/Interactable.h"


AAQBaseCharacter::AAQBaseCharacter()
{
	CharacterAttributeComponent = CreateDefaultSubobject<UCharacterAttributeComponent>(TEXT("CharacterAttributes"));
}

void AAQBaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	CharacterAttributeComponent->OnDeathEvent.AddUObject(this, &AAQBaseCharacter::OnDeath);
}

void AAQBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	TraceLineOfSight();
}

//float AAQBaseCharacter::GetHealthPercent() const
//{
//	return CharacterAttributeComponent->GetHealthPercent();
//}

const UCharacterAttributeComponent* AAQBaseCharacter::GetCharacterAttributesComponent() const
{
	return CharacterAttributeComponent;
}

void AAQBaseCharacter::Interact()
{
	if (LineOfSightObject.GetInterface())
	{
		LineOfSightObject->Interact(this);
	}
}

void AAQBaseCharacter::OnDeath()
{
	PlayAnimMontage(OnDeathAnimMontage);
	GetCharacterMovement()->DisableMovement();
	GetMesh()->SetCollisionProfileName(FName("Ragdoll"));
	GetMesh()->SetSimulatePhysics(true);
}

//�������������� � ���������||������� � �������
void AAQBaseCharacter::TraceLineOfSight()
{
	if (!IsPlayerControlled())
	{
		return;
	}
	
	FVector ViewLocation;
	FRotator ViewRotation;

	APlayerController* PlayerController = GetController<APlayerController>();
	PlayerController->GetPlayerViewPoint(ViewLocation, ViewRotation);

	FVector ViewDirection = ViewRotation.Vector();
	FVector TraceEnd = ViewLocation + ViewDirection * LineOfSightDistance;

	FHitResult HitResult;
	GetWorld()->LineTraceSingleByChannel(HitResult, ViewLocation, TraceEnd, ECC_Visibility);
	if (LineOfSightObject.GetObject() != HitResult.Actor)
	{
		LineOfSightObject = HitResult.Actor.Get();
		FName ActionName;
		if (LineOfSightObject.GetInterface())
		{
			ActionName = LineOfSightObject->GetActionEventName();
		}
		else
		{
			ActionName = NAME_None;
		}
	}
}
