// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Animation/AnimMontage.h"
#include "UObject/ScriptInterface.h"
#include "AQBaseCharacter.generated.h"

class IInteractable;
class UCharacterAttributeComponent;
UCLASS(Abstract, NotBlueprintable)
class UNREALQUEST_API AAQBaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AAQBaseCharacter();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	virtual void MoveForward(float Value) {};
	virtual void MoveRight(float Value) {};
	virtual void Turn(float Value) {};
	virtual void LookUp(float Value) {};

	//UFUNCTION(BlueprintCallable)
	//float GetHealthPercent() const;

	const UCharacterAttributeComponent* GetCharacterAttributesComponent() const;

	void Interact();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character | Components")
	class UCharacterAttributeComponent* CharacterAttributeComponent;

	virtual void OnDeath();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character | Animations")
	class UAnimMontage* OnDeathAnimMontage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character | Trace")
	float LineOfSightDistance = 500.0f;

	void TraceLineOfSight();

	UPROPERTY()
	TScriptInterface<IInteractable> LineOfSightObject;

};
