// Fill out your copyright notice in the Description page of Project Settings.


#include "AQBaseCharacterAnimInstance.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "../AQBaseCharacter.h"

void UAQBaseCharacterAnimInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();
	checkf(TryGetPawnOwner()->IsA<AAQBaseCharacter>(), TEXT("UAQBaseCharacterAnimInstance::NativeBeginPlay() can be used only with AAQBaseCharacter"));
	CachedBaseCharacter = StaticCast<AAQBaseCharacter*>(TryGetPawnOwner());
}

void UAQBaseCharacterAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);
	if (!CachedBaseCharacter.IsValid())
	{
		return;
	}
	UCharacterMovementComponent* CharacterMovement = CachedBaseCharacter->GetCharacterMovement();
	Speed = CharacterMovement->Velocity.Size();
	bIsFalling = CharacterMovement->IsFalling();
}
