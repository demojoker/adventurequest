// Fill out your copyright notice in the Description page of Project Settings.


#include "AQPlayerController.h"
#include "../AQBaseCharacter.h"

void AAQPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	CachedBaseCharacter = Cast<AAQBaseCharacter>(InPawn);
}

void AAQPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAxis("MoveForward", this, &AAQPlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AAQPlayerController::MoveRight);
	InputComponent->BindAxis("Turn", this, &AAQPlayerController::Turn);
	InputComponent->BindAxis("LookUp", this, &AAQPlayerController::LookUp);

	InputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &AAQPlayerController::Jump);
	InputComponent->BindAction("Interact", EInputEvent::IE_Pressed, this, &AAQPlayerController::Interact);
}

void AAQPlayerController::MoveForward(float Value)
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->MoveForward(Value);
	}
}

void AAQPlayerController::MoveRight(float Value)
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->MoveRight(Value);
	}
}

void AAQPlayerController::Turn(float Value)
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->Turn(Value);
	}
}

void AAQPlayerController::LookUp(float Value)
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->LookUp(Value);
	}
}

void AAQPlayerController::Jump()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->Jump();
	}
}

void AAQPlayerController::Interact()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->Interact();
	}
}
