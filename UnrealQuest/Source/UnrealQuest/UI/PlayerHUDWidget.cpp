// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerHUDWidget.h"
#include "../Character/AQBaseCharacter.h"
#include "../Components/CharacterComponents/CharacterAttributeComponent.h"

float UPlayerHUDWidget::GetHealthPercent() const
{
	float Result = 1.0f;

	APawn* Pawn = GetOwningPlayerPawn();
	AAQBaseCharacter* Character = Cast<AAQBaseCharacter>(Pawn);

	if (IsValid(Character))
	{
		const UCharacterAttributeComponent* CharacterAttributes = Character->GetCharacterAttributesComponent();
		Result = CharacterAttributes->GetHealthPercent();
	}
	return Result;
}
