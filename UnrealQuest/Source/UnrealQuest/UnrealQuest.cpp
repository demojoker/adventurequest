// Copyright Epic Games, Inc. All Rights Reserved.

#include "UnrealQuest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UnrealQuest, "UnrealQuest" );
