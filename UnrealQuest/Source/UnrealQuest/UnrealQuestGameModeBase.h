// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnrealQuestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UNREALQUEST_API AUnrealQuestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
